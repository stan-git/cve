import json
import requests
from datetime import datetime, timedelta


def load_keywords():
    pass


def get_cve(start_date, keyword, res):
    return requests.get("https://services.nvd.nist.gov/rest/json/cves/1.0?resultsPerPage={}&pubStartDate="
                        "{}%20UTC&keyword={}".format(res, start_date, keyword))


if __name__ == '__main__':
    now = datetime.now()-timedelta(1)   # yesterday
    date = now.strftime("%Y-%m-%dT00:00:00:000")
    print("[*] Current Time =", date)

    key_file = json.load(open("keys.dat"))
    for key in key_file["keys"]:
        cve_json = get_cve(date, key, 2000).json()

        if cve_json["resultsPerPage"] > 0:
            key_file["keys"][key] = cve_json["result"]
            print(key_file["keys"][key])

        print("{} : Es gibt {} neue Schwachstellen.\n".format(key, cve_json["resultsPerPage"]))
